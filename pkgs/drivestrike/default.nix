{ lib, stdenv, makeWrapper, dpkg, fetchurl,
  glib, glib-networking, wrapGAppsHook,
  autoPatchelfHook, libsoup, dmidecode }:
let
  version = "2.1.22-31";
  rpath = stdenv.lib.makeLibraryPath [

  ] + ":${stdenv.cc.cc.lib}/lib64";

  src = 
	if stdenv.hostPlatform.system == "x86_64-linux" then
	  fetchurl {
		urls = [ "https://app.drivestrike.com/static/apt/pool/main/d/drivestrike/drivestrike_${version}_amd64.deb" ];
		sha256 = "01bnh9s2mljrz95r1p5jxx4kv2dj5n5ngmp1y8vggl7zvz59fp3f";
	  }
	else
	  throw "Drivestrike is not supported on ${stdenv.hostPlatform.system}";
in stdenv.mkDerivation {
  pname = "drivestrike";
  inherit version;

  system = "x86_64-linux";

  inherit src;

  nativeBuildInputs = [ autoPatchelfHook wrapGAppsHook glib glib-networking makeWrapper ];

  buildInputs = [ dpkg libsoup dmidecode ];

  dontUnpack = true;

  installPhase = ''
	mkdir -p $out
	dpkg -x $src $out
	cp -av $out/usr/* $out
	rm -rf $out/usr

	chmod -R g-w $out
  '';

  postFixup = ''
	wrapProgram $out/bin/drivestrike \
	--prefix PATH ":" "${lib.makeBinPath [ dmidecode ]}"
  '';
}
