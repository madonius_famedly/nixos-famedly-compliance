{ pkgs ? import <nixpkgs> {} }:
{
  lib = import ./lib { inherit pkgs; };
  modules = import ./modules;
  overlays = import ./overlays;

  drivestrike = pkgs.callPackage ./pkgs/drivestrike {};
  nixosModules.drivestrike.imports = [ ./modules/drivestrike.nix ];
  osquery = pkgs.callPackage ./pkags/osquery {};
  nixosModules.osquery.imports = [ ./modules/osquery.nix ];
}
