{
  description = "Famedly's Nix Repository to ensure device compliance";
  
  inputs = {
	nixpkgs-osquery.url = "github:jdbaldry/nixpkgs/add/osquery";
  };

  outputs = { self, nixpkgs-osquery, ... }: {
	# drivestrike
    nixosModules.driveStrike.imports = [ ./modules/drivestrike ];

	# osquery
	nixosModules.osquery = {
	  imports = [
		(nixpkgs-osquery + "/nixos/modules/services/monitoring/osquery/")
		(nixpkgs-osquery + "/nixos/modules/rename.nix")
	  ];
	  nixpkgs.overlays = [( self: super: {
		  osquery = nixpkgs-osquery.legacyPackages.${self.stdenv.system}.osquery;
		}
	  )];
	  disabledModules = [ "rename.nix" ];
	};

    overlays.default = self: super: {
	  drivestrike = self.callPackage ./pkgs/drivestrike {};
    };
  };
}
